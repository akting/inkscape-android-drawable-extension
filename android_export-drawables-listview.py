#!/usr/bin/env python
"""
android_export-drawables-listview.py

Copyright (C) 2012 Akting Ingeniaritza, info@akting.eu

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
"""

import androidexport

class ListView(androidexport.Export):
    def effect(self):
    	base_icon_size = 32
        output_versions = [
            {'name': 'drawable-ldpi', 'width': base_icon_size*.75, 'height': base_icon_size*.75},
            {'name': 'drawable-mdpi', 'width': base_icon_size, 'height': base_icon_size},
            {'name': 'drawable-hdpi', 'width': base_icon_size*1.5, 'height': base_icon_size*1.5},
            {'name': 'drawable-xhdpi', 'width': base_icon_size*2, 'height': base_icon_size*2},
            {'name': 'drawable-xxhdpi', 'width': base_icon_size*3, 'height': base_icon_size*3},
            {'name': 'drawable-xxxhdpi', 'width': base_icon_size*4, 'height': base_icon_size*4},
        ]

    	self.export(output_versions)

if __name__ == '__main__':
    listView = ListView()
    listView.affect()
